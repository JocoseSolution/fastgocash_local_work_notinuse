﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_senderdetail_model.ascx.cs" Inherits="DMT_Manager_User_Control_uc_senderdetail_model" %>

<button type="button" class="btn btn-info btn-lg hidden addbenificiarydetail" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#FormBeneficiary"></button>
<div class="modal fade" id="FormBeneficiary" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container" id="BeneficiarySectionForm">
                    <div class="login-signup-page mx-auto">
                        <h3 class="font-weight-400 text-center">New Beneficiary Detail
                                <button type="button" id="btnbenformclose" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h3>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <div class="form-group form-validation">
                                <input id="txtBenAccountNo" type="text" class="form-control" placeholder="Enter Bank Account Number" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>
                            <div class="form-group form-validation">
                                <select id="ddlBindBankDrop" class="form-control">
                                    <option value="">Select Bank</option>
                                </select>
                                <p id="typeofifsc" class="text-warning text-right"></p>
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenIFSCNo" type="text" class="form-control" placeholder="Enter IFSC Number" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenPerName" type="text" class="form-control" placeholder="Enter Beneficiary Name" />
                            </div>
                            <div class="form-group form-validation">
                                <input id="txtBenPerMobile" type="text" class="form-control" placeholder="Enter Beneficiary Mobile Number" maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                            </div>
                            <span id="btnBenRegistration" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return SubmitBeneficiaryDetail();">Submit</span>
                            <p id="benerrormessage" class="text-danger"></p>
                            <h6 id="bensuccessmessage" class="text-success"></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg popupopenclass hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#PopupMsg"></button>
<div class="modal fade" id="PopupMsg" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 15% auto!important;">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-header" style="padding: 12px;">
                <h5 class="modal-title popupheading"></h5>
            </div>
            <div class="modal-body" style="padding-left: 20px!important;">
                <h6 class="popupcontent"></h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden benificierypopupclass" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#BenificieryDetail"></button>
<div class="modal fade" id="BenificieryDetail" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 10% auto!important;">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header" style="padding: 12px;">
                <h5 class="modal-title popupheading text-success"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;Benificiery Detail</h5>
            </div>
            <div class="modal-body" style="padding-left: 20px!important;" id="BenBodyContent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden delbendetail" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#DeleteBenDetail"></button>
<div class="modal fade" id="DeleteBenDetail" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 10% auto!important;">
        <div id="ConfirmBenDelete" class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding: 30px!important;">
                <h6>Are you sure want to delete this benificiary detail?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnDelConfirmed" class="btn btn-sm btn-success" style="padding: 0.5rem 1rem!important;" onclick="ConfirmDeleteDen();">YES</button>
                <button type="button" class="btn btn-sm btn-danger" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">NO</button>
            </div>
        </div>
        <div id="BenDeleteOtpSent" class="modal-content hidden" style="text-align: center; border-radius: 5px !important;">
            <div class="container">
                <div class="login-signup-page mx-auto">
                    <h3 class="font-weight-400 text-center text-success">
                        <button type="button" id="BenDelCloseClick" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </h3>
                    <p class="lead text-center text-success">Otp sent successfully to your mobile number.</p>
                    <div class="bg-light shadow-md rounded p-4 mx-2">
                        <div class="form-group form-validation">
                            <label style="float: left; font-size: 1rem;">Enter OTP Number</label>
                            <input type="text" id="txtBenDeleteOtp" class="form-control" placeholder="Enter OTP Number." onkeypress="return isNumberValidationPrevent(event);" />
                        </div>
                        <span id="btnBenDelVarification" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="javascript: return BenDelVarification();">Verify</span>
                        <p id="bendelvariyerror" class="text-danger"></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="DeleteSuccessfully" class="modal-content hidden" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding: 30px!important;">
                <h6 id="bendeletemsg"></h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-danger" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-info btn-lg hidden updateaddresspopup" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#UpdateAddressPopup"></button>
<div class="modal fade" id="UpdateAddressPopup" role="dialog">
    <div class="modal-dialog modal-md" style="margin: 5% auto!important;">
        <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
            <div class="modal-body" style="padding-left: 20px!important;">
                <div class="container">
                    <div class="login-signup-page mx-auto">
                        <h4 class="font-weight-400 text-center">Update Current / Local Address
                                <button type="button" id="UpdateAddressClose" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </h4>
                        <div class="bg-light shadow-md rounded p-4 mx-2">
                            <div class="form-group form-validation">
                                <textarea id="txtUpdateCurrLocalAddress" class="form-control" placeholder="Current / Local Address"></textarea>
                            </div>
                            <span id="btnUpdateCurrLocalAddress" style="cursor: pointer;" class="btn btn-primary btn-block my-4" onclick="return UpdateCurrLocalAddress();">Update Address</span>
                            <p id="updatemsg"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
