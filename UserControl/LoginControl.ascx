﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LoginControl.ascx.vb"
    Inherits="UserControl_LoginControl" %>




<style type="text/css">
    /*---- NUMBER OF SLIDE CONFIGURATION ----*/
    .wrapper {
        max-width: 60em;
        margin: 4.5em auto;
        position: relative;
    }

    input {
        display: none;
    }

    .inner {
        width: 500%;
        line-height: 0;
    }

    article {
        width: 20%;
        float: left;
        position: relative;
    }

        article img {
            width: 100%;
            height: 405px;
        }

    /*---- SET UP CONTROL ----*/
    .slider-prev-next-control {
        height: 50px;
        position: absolute;
        top: 50%;
        width: 100%;
        -webkit-transform: translateY(-50%);
        -moz-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        -o-transform: translateY(-50%);
        transform: translateY(-50%);
    }

        .slider-prev-next-control label {
            display: none;
            width: 40px;
            height: 40px;
            border-radius: 50%;
            background: #fff;
            opacity: 0.7;
        }

            .slider-prev-next-control label:hover {
                opacity: 1;
            }

    .slider-dot-control {
        position: absolute;
        width: 100%;
        bottom: 0;
        text-align: center;
    }

        .slider-dot-control label {
            cursor: pointer;
            border-radius: 5px;
            display: inline-block;
            width: 10px;
            height: 10px;
            background: #bbb;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

            .slider-dot-control label:hover {
                background: #ccc;
                border-color: #777;
            }

    /* Info Box */
    .info {
        position: absolute;
        font-style: italic;
        line-height: 20px;
        opacity: 0;
        color: #000;
        text-align: left;
        -webkit-transition: all 1000ms ease-out 600ms;
        -moz-transition: all 1000ms ease-out 600ms;
        transition: all 1000ms ease-out 600ms;
    }

        .info h3 {
            color: #fcfff4;
            margin: 0 0 5px;
            font-weight: normal;
            font-size: 1.5em;
            font-style: normal;
        }

        .info.top-left {
            top: 30px;
            left: 30px;
        }

        .info.top-right {
            top: 30px;
            right: 30px;
        }

        .info.bottom-left {
            bottom: 30px;
            left: 30px;
        }

        .info.bottom-right {
            bottom: 30px;
            right: 30px;
        }

    /* Slider Styling */
    .slider-wrapper {
        width: 100%;
        overflow: hidden;
        border-radius: 5px;
        box-shadow: 1px 1px 4px #666;
        background: #fff;
        background: #fcfff4;
        -webkit-transform: translateZ(0);
        -moz-transform: translateZ(0);
        -ms-transform: translateZ(0);
        -o-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-transition: all 500ms ease-out;
        -moz-transition: all 500ms ease-out;
        transition: all 500ms ease-out;
    }

        .slider-wrapper .inner {
            -webkit-transform: translateZ(0);
            -moz-transform: translateZ(0);
            -ms-transform: translateZ(0);
            -o-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-transition: all 800ms cubic-bezier(0.77, 0, 0.175, 1);
            -moz-transition: all 800ms cubic-bezier(0.77, 0, 0.175, 1);
            transition: all 800ms cubic-bezier(0.77, 0, 0.175, 1);
        }

    /*---- SET POSITION FOR SLIDE ----*/
    #slide1:checked ~ .slider-prev-next-control label:nth-child(2)::after, #slide2:checked ~ .slider-prev-next-control label:nth-child(3)::after, #slide3:checked ~ .slider-prev-next-control label:nth-child(4)::after, #slide4:checked ~ .slider-prev-next-control label:nth-child(5)::after, #slide5:checked ~ .slider-prev-next-control label:nth-child(1)::after, #slide2:checked ~ .slider-prev-next-control label:nth-child(1)::after, #slide3:checked ~ .slider-prev-next-control label:nth-child(2)::after, #slide4:checked ~ .slider-prev-next-control label:nth-child(3)::after, #slide5:checked ~ .slider-prev-next-control label:nth-child(4)::after, #slide1:checked ~ .slider-prev-next-control label:nth-child(5)::after {
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        margin: 0;
        line-height: 38px;
        font-size: 3em;
        display: block;
        color: #777;
    }

    #slide1:checked ~ .slider-prev-next-control label:nth-child(2)::after, #slide2:checked ~ .slider-prev-next-control label:nth-child(3)::after, #slide3:checked ~ .slider-prev-next-control label:nth-child(4)::after, #slide4:checked ~ .slider-prev-next-control label:nth-child(5)::after, #slide5:checked ~ .slider-prev-next-control label:nth-child(1)::after {
        content: "\f105";
        padding-left: 15px;
    }

    #slide1:checked ~ .slider-prev-next-control label:nth-child(2), #slide2:checked ~ .slider-prev-next-control label:nth-child(3), #slide3:checked ~ .slider-prev-next-control label:nth-child(4), #slide4:checked ~ .slider-prev-next-control label:nth-child(5), #slide5:checked ~ .slider-prev-next-control label:nth-child(1) {
        display: block;
        float: right;
        margin-right: 5px;
    }

    #slide2:checked ~ .slider-prev-next-control label:nth-child(1), #slide3:checked ~ .slider-prev-next-control label:nth-child(2), #slide4:checked ~ .slider-prev-next-control label:nth-child(3), #slide5:checked ~ .slider-prev-next-control label:nth-child(4), #slide1:checked ~ .slider-prev-next-control label:nth-child(5) {
        display: block;
        float: left;
        margin-left: 5px;
    }

        #slide2:checked ~ .slider-prev-next-control label:nth-child(1)::after, #slide3:checked ~ .slider-prev-next-control label:nth-child(2)::after, #slide4:checked ~ .slider-prev-next-control label:nth-child(3)::after, #slide5:checked ~ .slider-prev-next-control label:nth-child(4)::after, #slide1:checked ~ .slider-prev-next-control label:nth-child(5)::after {
            content: "\f104";
            padding-left: 8px;
        }

    #slide1:checked ~ .slider-dot-control label:nth-child(1), #slide2:checked ~ .slider-dot-control label:nth-child(2), #slide3:checked ~ .slider-dot-control label:nth-child(3), #slide4:checked ~ .slider-dot-control label:nth-child(4), #slide5:checked ~ .slider-dot-control label:nth-child(5) {
        background: #333;
    }

    #slide1:checked ~ .slider-wrapper article:nth-child(1) .info, #slide2:checked ~ .slider-wrapper article:nth-child(2) .info, #slide3:checked ~ .slider-wrapper article:nth-child(3) .info, #slide4:checked ~ .slider-wrapper article:nth-child(4) .info, #slide5:checked ~ .slider-wrapper article:nth-child(5) .info {
        opacity: 1;
    }

    #slide1:checked ~ .slider-wrapper .inner {
        margin-left: 0%;
    }

    #slide2:checked ~ .slider-wrapper .inner {
        margin-left: -100%;
    }

    #slide3:checked ~ .slider-wrapper .inner {
        margin-left: -200%;
    }

    #slide4:checked ~ .slider-wrapper .inner {
        margin-left: -300%;
    }

    #slide5:checked ~ .slider-wrapper .inner {
        margin-left: -400%;
    }

    /*---- TABLET ----*/
    @media only screen and (max-width: 850px) and (min-width: 450px) {
        .slider-wrapper {
            border-radius: 0;
        }
    }
    /*---- MOBILE----*/
    @media only screen and (max-width: 450px) {
        .slider-wrapper {
            border-radius: 0;
        }

            .slider-wrapper .info {
                opacity: 0;
            }
    }
</style>

<style type="text/css">

    @media only screen and (max-width: 500px) {
        .slid-er {
            display:none;
        }

        .formbox {
    width: 100% !important;
    background: #fff;
    padding: 30px;
    margin-top: 50px;
    border-radius: 4px;
    color: #ff414d;
     margin-left: 0px !important; 
    box-shadow: 0 0 25px rgba(0,0,0,.3);
}
    }

</style>


<asp:Login ID="UserLogin" runat="server">


    <TextBoxStyle />
    <LoginButtonStyle />
    <LayoutTemplate>

        <style>
            .featured-box2 {
                margin-left: auto;
                margin-right: auto;
                position: relative;
                border: 1px solid #ccc;
                text-align: center;
                padding: 6px;
                /*box-shadow: 0px 0px 4px #ccc;
                border-radius: 5px;*/
            }

                .featured-box2:hover {
                    border: 1px solid #ebeded;
                    -webkit-box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                    box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                }

            table {
                border-collapse: collapse;
                width: 100% !important;
            }

            .textboxcss {
                padding: 9px 10px !important;
                width: 100% !important;
                margin-bottom: 10px !important;
            }

            .dcv {
                min-height: 500px;
                background-image: url(../../DMT-Manager/images/bg/Balloon-Festival-114.jpg);
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .contenthed {
                border: 1px solid #0d6d86;
                padding: 60px;
                font-size: 59px;
                margin-top: 80px;
                margin-left: 20px;
                border-radius: 4px;
                color: #13688f;
            }

            .featured-box {
                box-sizing: border-box;
                margin-left: auto;
                margin-right: auto;
                padding: 10px;
                position: relative;
                border: 1px solid #ccc;
                text-align: center;
            }

                .featured-box:hover {
                    border: 1px solid #ebeded;
                    -webkit-box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                    box-shadow: 0px 5px 1.5rem rgba(0, 0, 0, 0.15);
                }

            .formbox {
                width: 75%;
                background: #fff;
                padding: 30px;
                margin-top: 50px;
                border-radius: 4px;
                color: #ff414d;
                margin-left: 62px;
                box-shadow: 0 0 25px rgba(0,0,0,.3);
            }

            .btn:hover {
                color: #ffffff !important;
                background: #106987 !important;
                text-decoration: none;
            }

            .btnwidth {
                background: #106987 !important;
                width: 100% !important;
                font-size: 18px !important;
            }

            .bg-primary, .badge-primary {
                background-color: #526681 !important;
            }

            .profileimage {
                width: 170px;
                height: 170px;
                border: 1px solid #ccc;
                padding: 5px;
                background: #ffffff;
                border-radius: 100px;
                margin-top: -10px;
                margin-left: 80px;
            }
        </style>

        <div id="content">

            <div class="dcv">
                <div class="container">
                    <div class="row">
                          
                        <div class="col-sm-6 slid-er">
    <div class="wrapper">
  <input checked type=radio name="slider" id="slide1" />
  <input type=radio name="slider" id="slide2" />
  <input type=radio name="slider" id="slide3" />
  <input type=radio name="slider" id="slide4" />
  <input type=radio name="slider" id="slide5" />

  <div class="slider-wrapper">
    <div class=inner>
      <article>
        <div class="info top-left">
          <%--<h3>Malacca</h3>--%></div>
        <img src="https://g.foolcdn.com/editorial/images/563759/travel-airport-scene.jpg" />
      </article>

      <article>
        <div class="info bottom-right">
          <%--<h3>Cameron Highland</h3>--%></div>
        <img src="https://media.istockphoto.com/photos/travel-planning-concept-on-map-picture-id891573112?k=6&m=891573112&s=612x612&w=0&h=4FMEW6gJadYGC5yMBeolS4rc7EzZa9_2DU1Y3-Ma7a4=" />
      </article>

      <article>
        <div class="info bottom-left">
         <%-- <h3>New Delhi</h3>--%></div>
        <img src="https://www.aircraftcompare.com/wp-content/uploads/2019/12/View-from-economy-class-seat-in-regional-jet.jpg" />
      </article>

      <article>
        <div class="info top-right">
          <%--<h3>Ladakh</h3>--%></div>
        <img src="https://farm8.staticflickr.com/7367/27980898905_72d106e501_h_d.jpg" />
      </article>

      <article>
        <div class="info bottom-left">
          <%--<h3>Nubra Valley</h3>--%></div>
        <img src="https://farm8.staticflickr.com/7356/27980899895_9b6c394fec_h_d.jpg" />
      </article>
    </div>
    <!-- .inner -->
  </div>
  <!-- .slider-wrapper -->

  <div class="slider-prev-next-control">
    <label for=slide1></label>
    <label for=slide2></label>
    <label for=slide3></label>
    <label for=slide4></label>
    <label for=slide5></label>
  </div>
  <!-- .slider-prev-next-control -->

  <div class="slider-dot-control">
    <label for=slide1></label>
    <label for=slide2></label>
    <label for=slide3></label>
    <label for=slide4></label>
    <label for=slide5></label>
  </div>
  <!-- .slider-dot-control -->
</div>
                        </div>




                        <div class="col-sm-6">                           
                           <div class="col-sm-12 formbox">
                                <h4 class="text-center">Welcome to FastgoCash, a growing b2b Travel Portal</h4>
                            <p class="text-center"><span class="fa fa-sign-in"></span>&nbsp;
                                Sign in to continue
                            </p>
                               <div class="col-sm-12">
                                    <h6 class="mb-0 text-sm">User Id</h6>
                                     <asp:TextBox runat="server" class="mb-4 form-control textboxcss" ID="UserName" placeholder="Enter a valid User Id"></asp:TextBox>
                               </div>
                                 <div class="col-sm-12">
                                     <h6 class="mb-0 text-sm">Password</h6>
                                        <asp:TextBox ID="Password"   TextMode="Password" placeholder="Enter password" Class="form-control textboxcss" runat="server"></asp:TextBox>                            
                                 </div>                               
                                <div class="col-sm-12 ">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="custom-control custom-checkbox custom-control-inline"> <input id="chk1" type="checkbox" name="chk" class="custom-control-input"> <label for="chk1" class="custom-control-label text-sm">Remember me</label> </div>
                                        </div>
                                        <div class="col-sm-6">
                                               <a href="../ForgotPassword.aspx" class="ml-auto mb-0 text-sm">Forgot Password?</a>
                                        </div>
                                    </div>
                                          
                                       </div>
                               <div class="col-sm-12  form-group">                                   
                                   <asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-blue text-center btnwidth"/>
                               </div>
                               <div class="col-sm-12">                                  
                                            <small class="font-weight-bold" style="font-size:14px !important;">Don't have an account? <a href="../regs_new.aspx" class="text-danger ">Register With Us</a></small>                                      
                               </div>                         
                        
                    </div>                             
                        </div>
                    </div>
                </div>
            </div>


           
         
      


        </div>





        <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" style="width:143% ;display:none;">  <%--style="width:161%;"  class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto"--%>
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row"> <img src="Images/gallery/logo(ft).png" class="logo"> </div>
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="https://mobilerechargesoftware.co.in/wp-content/uploads/DMT-1.png" class="image"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                    <div class="row mb-4 px-3">
                        <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                        <div class="facebook text-center mr-3">
                            <div class="fa fa-facebook"></div>
                        </div>
                        <div class="twitter text-center mr-3">
                            <div class="fa fa-twitter"></div>
                        </div>
                        <div class="linkedin text-center mr-3">
                            <div class="fa fa-linkedin"></div>
                        </div>
                    </div>
                    <div class="row px-3 mb-4">
                        <div class="line"></div> <small class="or text-center">Or</small>
                        <div class="line"></div>
                    </div>
                   
                </div>
            </div>
        </div>
       
    </div>
</div>






        <div id="main-wrapper2" class="h-100" style="width:115%;display:none;" >
  <div class="container-fluid px-0 h-100">
    <div class="row no-gutters h-100">
    
      <div class="col-md-6">
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-primary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('New_Style/images/bg/image-3.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row no-gutters">
              <div class="col-10 col-lg-9 mx-auto">
                <div class="logo mt-5 mb-5 mb-md-0"> <a class="d-flex" href="home.aspx" title="fastgocash"><img src="Images/gallery/logo(ft).png" alt="fastgocash" style="width: 200px;"></a> </div>
              </div>
            </div>
              <div class="row no-gutters my-auto">
                <div class="col-10 col-lg-9 mx-auto">
                  <h1 class="text-11 text-white mb-4">Welcome back!</h1>
                  <p class="text-4 text-white line-height-4 mb-5">We are glad to see you again! Instant deposits, withdrawals &amp; payouts trusted by millions worldwide.</p>
                </div>
              </div>
          </div>
        </div>
      </div>
   

        


      <div class="col-md-6 d-flex align-items-center" ">
        <div class="container my-4">
          <div class="row">
            <div class="col-11 col-lg-9 col-xl-8 mx-auto">
              <h3 class="font-weight-400 mb-4">Log In</h3>
              <form id="loginForm2" method="post">
                <div class="form-group">
                  <label for="emailAddress">User Id</label>
                 <%-- <input type="email" class="form-control" id="emailAddress" required="" placeholder="Enter Your Email">--%>
                   <%-- <asp:TextBox runat="server" class="form-control" ID="UserName" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Id is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
							<%--<label class="">User Name</label>--%>
                </div>
                <div class="form-group">
                  <label for="loginPassword">Password</label>
                 <%-- <input type="password" class="form-control" id="loginPassword" required="" placeholder="Enter Password">--%>
                    <%--<asp:TextBox ID="Password" class="form-control"  TextMode="Password" runat="server"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="UserLogin">*</asp:RequiredFieldValidator>--%>
                </div>
                <div class="row">
                  <div class="col-sm">
                    <div class="form-check custom-control custom-checkbox">
                      <input id="remember-me" name="remember" class="custom-control-input" type="checkbox">
                      <label class="custom-control-label" for="remember-me">Remember Me</label>
                    </div>
                  </div>
                  <div class="col-sm text-right"><a class="btn-link" href="#">Forgot Password</a></div>
                </div>
                <%--<button class="btn btn-primary btn-block my-4" type="submit">Login</button>--%>
                  <%--<asp:Button runat="server" ID="LoginButton" OnClick="LoginButton_Click" Text="Sign In" class="btn btn-primary btn-block my-4"/>--%>
              </form>
              <p class="text-3 text-center text-muted">Don't have an account <a class="btn-link" href="Regs.aspx">Sign Up</a></p>
            </div>
          </div>
        </div>
      </div>
      <!-- Login Form End -->
    </div>
  </div>
</div>
        

         


        <div class="large-12 medium-12 small-12" style="display:none;">
            <div class="col-md-12  userway "><i class="fa fa-user-circle" aria-hidden="true"></i></div>
            <div class="lft f16" style="display: none;">

                Login Here As
                <asp:DropDownList ID="ddlLogType" runat="server">
                    <asp:ListItem Value="C">Customer</asp:ListItem>
                    <asp:ListItem Value="M">Management</asp:ListItem>
                </asp:DropDownList>
            </div>
         
            <div class="clear1">
            </div>

            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                </div>
               
            </div>
            <div class="form-group has-success has-feedback">

                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true" style="    font-size: 23px;" ></i></span>
                   
                </div>
         
            </div>



            <div class="large-4 medium-4 small-12 columns">
                <div class="clear1">
                </div>
       
                <br />

                <a href="../ForgotPassword.aspx" style="color:#ff0000">Forgot Password</a>
            </div>
            <div class="clear">
            </div>
            <div>
                <asp:Label ID="lblerror" Font-Size="10px" runat="server" ForeColor="Red"></asp:Label>
            </div>
            <div class="clear">
            </div>
        </div>
    </LayoutTemplate>
    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    <TitleTextStyle />
</asp:Login>



<script>
    var overlay = document.getElementById("overlay");

            // Buttons to 'switch' the page
            var openSignUpButton = document.getElementById("slide-left-button");
            var openSignInButton = document.getElementById("slide-right-button");

            // The sidebars
            var leftText = document.getElementById("sign-in");
            var rightText = document.getElementById("sign-up");

            // The forms
            var accountForm = document.getElementById("sign-in-info")
            var signinForm = document.getElementById("sign-up-info");

            // Open the Sign Up page
            openSignUp = () => {
                // Remove classes so that animations can restart on the next 'switch'
                leftText.classList.remove("overlay-text-left-animation-out");
                overlay.classList.remove("open-sign-in");
                rightText.classList.remove("overlay-text-right-animation");
                // Add classes for animations
                accountForm.className += " form-left-slide-out"
                rightText.className += " overlay-text-right-animation-out";
                overlay.className += " open-sign-up";
                leftText.className += " overlay-text-left-animation";
                // hide the sign up form once it is out of view
                setTimeout(function () {
                    accountForm.classList.remove("form-left-slide-in");
                    accountForm.style.display = "none";
                    accountForm.classList.remove("form-left-slide-out");
                }, 700);
                // display the sign in form once the overlay begins moving right
                setTimeout(function () {
                    signinForm.style.display = "flex";
                    signinForm.classList += " form-right-slide-in";
                }, 200);
            }

            // Open the Sign In page
            openSignIn = () => {
                // Remove classes so that animations can restart on the next 'switch'
                leftText.classList.remove("overlay-text-left-animation");
                overlay.classList.remove("open-sign-up");
                rightText.classList.remove("overlay-text-right-animation-out");
                // Add classes for animations
                signinForm.classList += " form-right-slide-out";
                leftText.className += " overlay-text-left-animation-out";
                overlay.className += " open-sign-in";
                rightText.className += " overlay-text-right-animation";
                // hide the sign in form once it is out of view
                setTimeout(function () {
                    signinForm.classList.remove("form-right-slide-in")
                    signinForm.style.display = "none";
                    signinForm.classList.remove("form-right-slide-out")
                }, 700);
                // display the sign up form once the overlay begins moving left
                setTimeout(function () {
                    accountForm.style.display = "flex";
                    accountForm.classList += " form-left-slide-in";
                }, 200);
            }

            // When a 'switch' button is pressed, switch page
            openSignUpButton.addEventListener("click", openSignUp, false);
            openSignInButton.addEventListener("click", openSignIn, false);
</script>
