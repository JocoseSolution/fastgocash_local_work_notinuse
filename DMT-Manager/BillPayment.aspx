﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="BillPayment.aspx.cs" Inherits="DMT_Manager_BillPayment" %>

<%@ Register Src="~/DMT-Manager/User_Control/ud_Mobile_Recharge.ascx" TagPrefix="uc1" TagName="ud_Mobile_Recharge" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_DTH_Recharge.ascx" TagPrefix="uc1" TagName="ud_DTH_Recharge" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_Electricity_Recharge.ascx" TagPrefix="uc1" TagName="ud_Electricity_Recharge" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_landline.ascx" TagPrefix="uc1" TagName="ud_landline" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_Insurance.ascx" TagPrefix="uc1" TagName="ud_Insurance" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_GAS.ascx" TagPrefix="uc1" TagName="ud_GAS" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_internet_isp.ascx" TagPrefix="uc1" TagName="ud_internet_isp" %>
<%@ Register Src="~/DMT-Manager/User_Control/ud_water.ascx" TagPrefix="uc1" TagName="ud_water" %>
<%@ Register Src="~/DMT-Manager/User_Control/uc_smbp_transhistory.ascx" TagPrefix="uc1" TagName="uc_smbp_transhistory" %>
<%@ Register Src="~/DMT-Manager/User_Control/FastTag.ascx" TagPrefix="uc1" TagName="FastTag" %>
<%@ Register Src="~/DMT-Manager/User_Control/EmiLoan.ascx" TagPrefix="uc1" TagName="EmiLoan" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <link href="custom/css/datetime.css" rel="stylesheet" />
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />--%>

    <link href="../Custom_Design/css/search.css" rel="stylesheet" />
    <link href="../icofont/icofont.css" rel="stylesheet" />
    <link href="../icofont/icofont.min.css" rel="stylesheet" />


    <style type="text/css">
        #main-wrapper {
            background: #004c60 !important;
        }
    </style>
    <style>

        @media only screen and (max-width: 500px) {
            .bill {
                display:none;
            }
        }

        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 170px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

                .dropdown-content a:hover {
                    background-color: #ff5858;
                    color: #fff;
                }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown:hover .dropbtn {
            background-color: #3e8e41;
        }

        .progress-bar, .nav-pills .nav-link.active, .nav-pills .show > .nav-link, .dropdown-item.active, .dropdown-item:active {
            background-color: none !important;
        }
    </style>

    <div class="nav mob-tabs" >
             

                <div class="outer">
            <div class="inner actv_nw active">
                <a href="<%=ResolveUrl("Search.aspx")%>" aria-controls="home" role="tab" data-toggle="tab">
                    <i class="fli_n_icn"></i>
                    <span class="icn_ttl fw700 tab-mob">flights</span>
                </a>
            </div>

              <div class="inner">
                <a href="<%=ResolveUrl("/dmt-manager/senderindex.aspx")%>">
                    <i class="mon_n_icn"></i>
                    <span class="icn_ttl">DMT 1</span>
                </a>
            </div>	

                                   <div class="inner">
                <a href="<%=ResolveUrl("/dmt-manager/dmt-direct.aspx")%>">
                    <i class="mon_n_icn"></i>
                    <span class="icn_ttl">Direct</span>
                </a>
            </div>	

                          <div class="inner">
                <a href="<%=ResolveUrl("/dmt-manager/billpayment.aspx")%>">
                    <i class="uti_n_icn"></i>
                    <span class="icn_ttl">Utility</span>
                </a>
            </div>

  
            
            
            <div class="inner">
                <a href="<%=ResolveUrl("/Dash.aspx")%>">
                    <i class="das_n_icn"></i>
                    <span class="icn_ttl">Dashboard</span>
                </a>
            </div>	
            		
                    
         
		
        </div>


            </div>

    <br />
    <br />

    


    <div class="col-md-12" style="min-height: 300px;">
        <div class="">
            <div class="dropdown nav-pills" style="cursor: pointer;">
                <a class="nav-link" style="background-color: #ff5858; color: #fff;"><span class="bill">Recharge</span><img src="../icofont/power-bank.png" /></a>
                <div class="dropdown-content" style="border-radius: 5px; background: #fff;">
                    <a class="actiontype" data-valtext="mobile" id="firstTab-Pills" data-toggle="tab" href="#mobileTabPills" role="tab" aria-controls="firstTabPills" aria-selected="true"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Mobile</a>
                    <a class="actiontype" data-valtext="dth" id="secondTab-Pills" data-toggle="tab" href="#dthTabPills" role="tab" aria-controls="secondTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;DTH</a>
                </div>
            </div>

            <div class="dropdown nav-pills" style="cursor: pointer;">
                <a class="nav-link" style="background-color: #ff5858; color: #fff;"><span class="bill">Bill Payment</span><img src="../icofont/bill.png" /></a>
                <div class="dropdown-content" style="min-width: 300px!important; border-radius: 5px; background: #fff;">
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="actiontype" data-valtext="electricity" id="thirdTab-Pills" data-toggle="tab" href="#thirdTabPills" role="tab" aria-controls="thirdTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Electricity</a>
                            <a class="actiontype" data-valtext="landline" id="fourthTab-Pills" data-toggle="tab" href="#fourthTabPills" role="tab" aria-controls="fourthTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Landline</a>
                            <a class="actiontype" data-valtext="insurance" id="fifthTab-Pills" data-toggle="tab" href="#fifthTabPills" role="tab" aria-controls="fifthTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Insurance</a>
                        </div>
                        <div class="col-sm-6">
                            <a class="actiontype" data-valtext="gas" id="gasTab-Pills" data-toggle="tab" href="#gasTabPills" role="tab" aria-controls="gasTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Gas</a>
                            <a class="actiontype" data-valtext="internet" id="internetTab-Pills" data-toggle="tab" href="#internetTabPills" role="tab" aria-controls="internetTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Broadband</a>
                            <a class="actiontype" data-valtext="water" id="waterTab-Pills" data-toggle="tab" href="#waterTabPills" role="tab" aria-controls="waterTabPills" aria-selected="false"><i class="fa fa-hand-o-right" aria-hidden="true"></i>&nbsp;Water</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dropdown nav-pills" style="cursor: pointer;">
                <a class="actiontype nav-link" data-valtext="fasttag" id="FastTag-Pills" data-toggle="tab" href="#FastTagTabPills" role="tab" aria-controls="FastTagTabPills" aria-selected="false" style="background-color: #ff5858; color: #fff;"><span class="bill">FasTag</span><img src="../icofont/toll.png" /></a>
            </div>
            <div class="dropdown nav-pills" style="cursor: pointer;">
                <a class="actiontype nav-link" data-valtext="emi" id="Emi-Pills" data-toggle="tab" href="#EmiTabPills" role="tab" aria-controls="EmiTabPills" aria-selected="false" style="background-color: #ff5858; color: #fff;"><span class="bill">Loan RePayment</span><img src="../icofont/personal.png" /></a>
            </div>

            <div class="dropdown nav-pills" style="cursor: pointer; float: right;">
                <a class="actiontype nav-link" data-valtext="billtranshistory" id="transHistTab-Pills" data-toggle="tab" href="#transHistTabPills" role="tab" aria-controls="transHistTabPills" aria-selected="false" style="background-color: #ff5858; color: #fff;"><span class="bill">Transaction History</span><img src="../icofont/history.png" /></a>
            </div>
        </div>

        <%--<ul class="nav nav-pills" id="pillsmyTab" role="tablist">
            <li class="nav-item"><a class="nav-link actiontype active" data-valtext="mobile" id="firstTab-Pills" data-toggle="tab" href="#mobileTabPills" role="tab" aria-controls="firstTabPills" aria-selected="true" style="color: #fff !important;">Mobile Recharge</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="dth" id="secondTab-Pills" data-toggle="tab" href="#dthTabPills" role="tab" aria-controls="secondTabPills" aria-selected="false" style="color: #fff !important;">DTH</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="electricity" id="thirdTab-Pills" data-toggle="tab" href="#thirdTabPills" role="tab" aria-controls="thirdTabPills" aria-selected="false" style="color: #fff !important;">Electricity</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="landline" id="fourthTab-Pills" data-toggle="tab" href="#fourthTabPills" role="tab" aria-controls="fourthTabPills" aria-selected="false" style="color: #fff !important;">Landline</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="insurance" id="fifthTab-Pills" data-toggle="tab" href="#fifthTabPills" role="tab" aria-controls="fifthTabPills" aria-selected="false" style="color: #fff !important;">Insurance</a> </li>

            <li class="nav-item"><a class="nav-link actiontype" data-valtext="gas" id="gasTab-Pills" data-toggle="tab" href="#gasTabPills" role="tab" aria-controls="gasTabPills" aria-selected="false" style="color: #fff !important;">Gas</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="internet" id="internetTab-Pills" data-toggle="tab" href="#internetTabPills" role="tab" aria-controls="internetTabPills" aria-selected="false" style="color: #fff !important;">Broadband</a> </li>
            <li class="nav-item"><a class="nav-link actiontype" data-valtext="water" id="waterTab-Pills" data-toggle="tab" href="#waterTabPills" role="tab" aria-controls="waterTabPills" aria-selected="false" style="color: #fff !important;">Water</a> </li>
        </ul>--%>
        <br />
        <br />
        <div class="tab-content bg-light shadow-sm rounded py-4 mb-4" id="pillsmyTabContent" style="padding: 10px;">

            <div class="tab-pane fade active show" id="mobileTabPills" role="tabpanel" aria-labelledby="firstTab-Pills">
                <uc1:ud_Mobile_Recharge runat="server" ID="ud_Mobile_Recharge" />
            </div>

            <div class="tab-pane fade" id="dthTabPills" role="tabpanel" aria-labelledby="secondTab-Pills">
                <uc1:ud_DTH_Recharge runat="server" ID="ud_DTH_Recharge" />
            </div>

            <div class="tab-pane fade" id="thirdTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">
                <uc1:ud_Electricity_Recharge runat="server" ID="ud_Electricity_Recharge" />
            </div>

            <div class="tab-pane fade" id="fourthTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">
                <uc1:ud_landline runat="server" ID="ud_landline" />
            </div>

            <div class="tab-pane fade" id="fifthTabPills" role="tabpanel" aria-labelledby="thirdTab-Pills">
                <uc1:ud_Insurance runat="server" ID="ud_Insurance" />
            </div>

            <div class="tab-pane fade" id="gasTabPills" role="tabpanel" aria-labelledby="gasTab-Pills">
                <uc1:ud_GAS runat="server" ID="ud_GAS" />
            </div>

            <div class="tab-pane fade" id="internetTabPills" role="tabpanel" aria-labelledby="internetTab-Pills">
                <uc1:ud_internet_isp runat="server" ID="ud_internet_isp" />
            </div>

            <div class="tab-pane fade" id="waterTabPills" role="tabpanel" aria-labelledby="waterTab-Pills">
                <uc1:ud_water runat="server" ID="ud_water" />
            </div>
            <div class="tab-pane fade" id="FastTagTabPills" role="tabpanel" aria-labelledby="FastTag-Pills">
                <uc1:FastTag runat="server" ID="FastTag" />
            </div>
            <div class="tab-pane fade" id="EmiTabPills" role="tabpanel" aria-labelledby="Emi-Pills">
                <uc1:EmiLoan runat="server" ID="EmiLoan" />
            </div>

            <div class="tab-pane fade" id="transHistTabPills" role="tabpanel" aria-labelledby="transHistTab-Pills">
                <uc1:uc_smbp_transhistory runat="server" ID="uc_smbp_transhistory" />
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />

    <button type="button" class="btn btn-info btn-lg successmessage hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#SuccessMsg"></button>
    <div class="modal fade" id="SuccessMsg" role="dialog">
        <div class="modal-dialog modal-md" style="margin: 15% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-header" style="padding: 12px;">
                    <h5 class="modal-title modelheading"></h5>
                </div>
                <div class="modal-body" style="padding-left: 20px!important;">
                    <h6 class="sucessmsg"></h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <script src="custom/js/billpayment.js?v=2"></script>
    <script src="custom/js/common.js"></script>
    <script>
        $(document.body).on('click', ".commonDate", function (e) {
            $(this).datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true
            });
        });

        $('.commonDate').datepicker({
            dateFormat: "dd/mm/yy",
            showStatus: true,
            showWeeks: true,
            currentText: 'Now',
            autoSize: true,
            maxDate: -0,
            gotoCurrent: true,
            showAnim: 'blind',
            highlightWeek: true
        });
    </script>
</asp:Content>

