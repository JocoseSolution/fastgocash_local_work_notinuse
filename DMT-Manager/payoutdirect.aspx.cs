﻿using InstantPayServiceLib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DMT_Manager_payoutdirect : System.Web.UI.Page
{
    // private static string UserId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            string UserId = Session["UID"].ToString();
            Session["PDUID"] = Session["UID"].ToString();
        }
        else
        {
            Response.Redirect("/");
        }
    }

    [WebMethod]
    public static List<string> RemitterMobileSearch(string mobileno)
    {
        List<string> result = new List<string>();

        try
        {
            if (HttpContext.Current.Session["PDUID"].ToString() == HttpContext.Current.Session["UID"].ToString())
            {
                string UserId = HttpContext.Current.Session["PDUID"].ToString();
                if (!string.IsNullOrEmpty(UserId))
                {
                    string response = InstantPay_ApiService.GetRemitterDetail(mobileno, UserId);

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            dynamic dyData = dyResult.data;
                            dynamic remitter = dyData.remitter;

                            string respoMobile = remitter.mobile;
                            string respoRemttId = remitter.id;

                            HttpContext.Current.Session["RemtIdPayoutSearch"] = respoRemttId;

                            result.Add("success");
                            result.Add("/dmt-manager/payout_direct_fund_transfer.aspx?mobile=" + respoMobile + "&sender=" + respoRemttId);
                        }
                        else if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                        {
                            result.Add("registration");
                            result.Add("/dmt-manager/senderindex.aspx?page=registration");
                        }
                        else if (statusCode.ToLower() == "rnf" && statusMessage.ToLower() == "remitter not found")
                        {
                            result.Add("registration");
                            result.Add("/dmt-manager/senderindex.aspx?page=registration");
                        }
                        else if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "invalid mobile number")
                        {
                            result.Add("error");
                            result.Add(statusMessage);
                        }
                        else
                        {
                            result.Add("error");
                            result.Add(statusMessage);
                        }
                    }
                }
                else
                {
                    result.Add("reload");
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }
}