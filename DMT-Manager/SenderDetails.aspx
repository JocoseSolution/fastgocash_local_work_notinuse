﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="SenderDetails.aspx.cs" Inherits="DMT_Manager_dmt_SenderDetails" %>

<%@ Register Src="~/DMT-Manager/User_Control/uc_senderdetail_model.ascx" TagPrefix="uc1" TagName="uc_senderdetail_model" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <link href="custom/css/datetime.css" rel="stylesheet" />

    <style>
        .boxSender {
            background: #ff414d;
            width: 100%;
            margin-left: 3px;
            color: #fff;
        }

        .p_botm {
            margin-bottom: 1px;
            padding: 4px;
        }

        .pmargin {
            margin-bottom: 4px;
        }

        .boxSenderdetail {
            border: 1px solid #ccc;
            width: 100%;
            margin-left: 3px;
        }

        .tab {
            overflow: hidden;
            border: 1px solid #ff414d;
            background-color: #f9f9f9;
        }

            /* Style the buttons inside the tab */
            .tab a {
                background-color: inherit;
                float: left;
                border: none;
                outline: none;
                cursor: pointer;
                padding: 7px 32px;
                transition: 0.3s;
                font-size: 13px;
                text-align: center;
                color: #215821 !important
            }


                .tab a:hover {
                    background-color: #fff;
                    border: 1px solid #ccc;
                    border-bottom: none;
                    border-top: none;
                    color: #ff414d !important;
                }

                .tab a.active {
                    background-color: #fff;
                    border: 1px solid #ff414d;
                    border-bottom: none;
                    border-top: none;
                    color: #ff414d !important;
                }

        .tabcontent {
            display: none;
            padding: 6px 12px;
            /*border: 1px solid #ccc;*/
            border-top: none;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 1px solid #000000;
            background: #f1f5f6;
        }

        .table-sm th {
            padding: 3px;
            text-align: center;
            font-weight: 500;
        }

        .table-bordered th {
            border: 1px solid #000000;
        }

        .textboxamount {
            padding: 1px !important;
            /*width: 70% !important;*/
        }

        .table-sm td {
            padding: 10px;
            text-align: center;
            /*color: #828282;*/
        }

        .accountvalid {
            color: green;
            font-size: 22px;
        }

        .accountpending {
            color: #d47514;
            font-size: 22px;
        }

        .hidden {
            display: none;
        }

        .fltbtn {
            /*float: right;*/
            padding: 4px;
            margin-bottom: 4px;
            background: red;
            border: 1px solid red;
        }

        .scrolltable {
            overflow-y: auto;
            max-height: 600px;
        }

        .table thead th {
            position: sticky;
            top: 0;
            padding: 10px;
        }

        @media only screen and (min-width: 1200px) {
            .container {
                max-width: 1400px !important;
            }
        }
    </style>

    <div class="container">
        <div class="row">
            <div id="RemitterDetailsSection" class="col-sm-12" style="border: 1px solid #ccc; padding: 12px; background: #fff; margin-top: 12px; margin-bottom: 12px;">
                <%=RemitterDetail %>
            </div>

            <div class="col-sm-12" style="margin-top: 14px; background: #fff; margin-bottom: 14px; padding: 12px;">
                <div class="tab">
                    <a class="tablinks active" onclick="openCity(event, 'user')"><span class="fa fa-user fa-2x text-center"></span>&nbsp;<br />
                        <b>Receivers</b></a>
                    <a class="tablinks" onclick="openCity(event, 'history')" id="TransHistory"><span class="fa fa-history fa-2x"></span>
                        <br />
                        <b>Transaction History</b>
                    </a>                   
                </div>
                <div id="user" class="tabcontent" style="display: block;">
                    <div class="row">
                        <div class="pull-right">
                            <span id="createReceiver" style="cursor: pointer;" class="btn btn-sm btn-primary" onclick="return AddNewReceiver();">Add Receiver</span>
                        </div>
                    </div>
                    <br />
                    <div class="row scrolltable">
                        <table class="table  table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <%--<th>Relationship</th>--%>
                                    <th>Bank</th>
                                    <th>Status</th>
                                    <th>Account No</th>
                                    <th>IFSC Code</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody id="BeneficaryRowDetails">
                                <%=RemitterBenDetail %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="history" class="tabcontent">
                    <div class="row">
                        <input type="button" value="Show Filter" class="btn btn-sm btn-primary" id="filterbtn" onclick="ShowHideDiv(this);" />
                    </div>
                    <div class="col-sm-12 form-group filterbox hidden" id="sidebar" style="margin: 10px;">
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">From Date</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control commonDate" placeholder="dd/mm/yyyy" id="txtTransFromDate" name="txtTransFromDate" style="padding: 4px;" />
                            </div>
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">To Date</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control commonDate" placeholder="dd/mm/yyyy" id="txtTransToDate" name="txtTransToDate" style="padding: 4px;" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">Track ID</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="txtTransTrackId" id="txtTransTrackId" class="form-control" placeholder="Track ID" style="padding: 4px;" />
                            </div>
                            <div class="col-sm-1">Status</div>
                            <div class="col-sm-3">
                                <select id="ddlTransStatus" class="form-control" style="height: 36px!important; padding-top: 6px!important;">
                                    <option value="">All</option>
                                    <option value="successful">Success</option>
                                    <option value="under process">Under Process</option>
                                    <option value="failed">Failed</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <span class="btn btn-success btn-sm col-sm-7" style="padding: 4px;" id="btnTransFilter" onclick="TransFilter();">Search</span>
                                <span class="btn btn-danger btn-sm col-sm-3" style="padding: 4px;" id="btnClearTransFilter" onclick="ClearTransFilter();">Clear</span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row scrolltable">
                        <table class="table  table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Txn Date</th>
                                    <th>Order ID</th>
                                    <%--<th>Ref Txn ID</th>--%>
                                    <th>Track ID</th>
                                    <th>Mode</th>
                                    <th>Amount(₹)</th>
                                    <%--<th>Chg(₹)</th>--%>
                                    <th>Reciver</th>
                                    <th>Status</th>
                                    <th>Refund</th>
                                    <th>Recipt</th>
                                </tr>
                            </thead>
                            <tbody id="FundTransRowDetails"></tbody>
                        </table>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <uc1:uc_senderdetail_model runat="server" ID="uc_senderdetail_model" />

    <script src="custom/js/new_dmt.js?v=4"></script>    
    <script src="custom/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        function ShowHideDiv(btnPassport) {
            var dvPassport = document.getElementById("sidebar");
            if (btnPassport.value == "Show Filter") {
                $(".filterbox").removeClass("hidden")
                btnPassport.value = "Hide Filter";
            } else {
                btnPassport.value = "Hide Filter";
                $(".filterbox").addClass("hidden")
                btnPassport.value = "Show Filter";
            }
        }

        function PayoutShowHideDiv(btnPayout) {
            var dvPayout = document.getElementById("payoutsidebar");
            if (btnPayout.value == "Show Filter") {
                $(".pauoutfilterbox").removeClass("hidden")
                btnPayout.value = "Hide Filter";
            } else {
                btnPayout.value = "Hide Filter";
                $(".pauoutfilterbox").addClass("hidden")
                btnPayout.value = "Show Filter";
            }
        }

        $('.commonDate').datepicker({
            dateFormat: "dd/mm/yy",
            showStatus: true,
            showWeeks: true,
            currentText: 'Now',
            autoSize: true,
            maxDate: -0,
            gotoCurrent: true,
            showAnim: 'blind',
            highlightWeek: true
        });
    </script>
</asp:Content>

