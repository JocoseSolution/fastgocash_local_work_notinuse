﻿function ResetRegistrationForm() {
    $("#txtRegMobileNo").val("");
    $("#txtRegFirstName").val("");
    $("#txtRegLastName").val("");
    $("#txtRegPinCode").val("");
    $("#txtRegOtp").val("");
    $("#txtCurrLocalAddress").val("");
    $("#perrormessage").html("");
}

function DMTDRemitterMobileSearch() {
    $("#btnRegOtpVarification").html("Verify");
    $("#DMTDTokenExpired").html("");
    ResetRegistrationForm();
    var thisbutton = $("#btnDMTDRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtDMTDSenderMobileNo")) return !1;
    var mobileno = $("#txtDMTDSenderMobileNo").val();

    if (mobileno.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/dmt-direct.aspx/DMTDRemitterMobileSearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    if (data.d[0] == "success") { window.location.href = data.d[1]; }
                    else if (data.d[0] == "otpsent") {
                        $("#txtDMTDSenderMobileNo").val("");
                        $("#otperrormessage").html("");
                        regmobile = data.d[1];
                        $("#hdnRegRemtId").val(data.d[2]);

                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $("#RegOtpHeading").html("Sender already registered, Please verify sender.<br/>Otp sent successfully to your mobile number.");
                        $("#RegistMargin").removeAttr("style").css("margin", "8% auto");
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        StartCountDown(30);
                    }
                    else if (data.d[0] == "registration") {
                        ResetRegistrationForm();
                        $("#txtRegMobileNo").val(mobileno);
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        $("#txtDMTDSenderMobileNo").val("");
                    }
                    else if (data.d[0] == "error") {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", data.d[1], "#ff414d");
                        $(thisbutton).html("Search");
                    }
                    else if (data.d[0] == "reload") { window.location.reload(); }
                }
                else {
                    $("#DMTDTokenExpired").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", "Mobile number not valid.", "#ff414d");
    }
}

var regirsterremitter = null; var regmobile = null;
function SubmitRegRemitter() {
    $("#perrormessage").html("");
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRemtrRegistration");


    if (CheckFocusBlankValidation("txtRegMobileNo")) return !1;
    var mobile = $("#txtRegMobileNo").val();
    if (mobile.length != 10) { $("#txtRegMobileNo").focus(); return false; }
    if (CheckFocusBlankValidation("txtRegFirstName")) return !1;
    if (CheckFocusBlankValidation("txtRegLastName")) return !1;
    if (CheckFocusBlankValidation("txtRegPinCode")) return !1;
    if (CheckFocusBlankValidation("txtCurrLocalAddress")) return !1;

    var firstname = $("#txtRegFirstName").val();
    var lastname = $("#txtRegLastName").val();
    var pincode = $("#txtRegPinCode").val();
    var localadd = $("#txtCurrLocalAddress").val();

    if (mobile.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/dmt-direct.aspx/DMTDRemitterRegistration",
            data: '{mobile: ' + JSON.stringify(mobile) + ',firstname: ' + JSON.stringify(firstname) + ',lastname: ' + JSON.stringify(lastname) + ',pincode: ' + JSON.stringify(pincode) + ',localadd: ' + JSON.stringify(localadd) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#RegistMargin").removeAttr("style").css("margin", "10% auto");

                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $("#hdnRegRemtId").val(data.d[1]);
                        regirsterremitter = data.d[1];
                        regmobile = mobile;
                        StartCountDown(30);
                    }
                    else if (data.d[0] == "failed") {
                        $("#perrormessage").html(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#btnregclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                    }
                }
                else {
                    $("#perrormessage").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Register");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        $("#perrormessage").html("Mobile number should be 10 digits!");
    }
}

function SubmitRegOtpVerification() {
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRegOtpVarification");
    if (CheckFocusBlankValidation("txtRegOtp")) return !1;

    var mobile = regmobile;
    var enteredotp = $("#txtRegOtp").val();
    var hdnremtVal = $("#hdnRegRemtId").val();
    var remitterid = hdnremtVal != "" ? hdnremtVal : regirsterremitter;

    if (mobile != "" && enteredotp != "" && remitterid != "") {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/dmt-direct.aspx/DMTDRemitterVarification",
            data: '{mobile: ' + JSON.stringify(mobile) + ',remitterid: ' + JSON.stringify(remitterid) + ',otp: ' + JSON.stringify(enteredotp) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#btnregformclose").click();
                        window.location.href = data.d[1];
                        //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Remitter registration has been successfully completed.", "#28a745");
                    }
                    else if (data.d[0] == "failed") {
                        $("#otperrormessage").html(data.d[1]);
                    }
                    else {
                        $("#btnregformclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                    }
                }
                $(thisbutton).html("Verify");
                $("#txtRegOtp").val("");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

var _tick = null;
function StartCountDown(startSecond) {
    $("#btnResendOTP").addClass("hidden");
    $("#timersection").removeClass("hidden");

    clearInterval(_tick);
    $("#secRemaing").html("");
    $("#minRemaing").html("");
    $(".strcountdown").removeClass("hidden");
    var remSeconds = startSecond;
    var secondCounters = remSeconds % 60;
    function formarteNumber(number) { if (number < 10) return '0' + number; else return '' + number; }
    function startTick() {
        $("#secRemaing").text(formarteNumber(secondCounters));
        $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
        if (secondCounters == 0) { secondCounters = 60; }
        _tick = setInterval(function () {
            if (remSeconds > 0) {
                remSeconds = remSeconds - 1;
                secondCounters = secondCounters - 1;
                $("#secRemaing").text(formarteNumber(secondCounters));
                $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
                if (secondCounters == 0) { secondCounters = 60; }
            }
            else {
                clearInterval(_tick);
                ResetResend();
            }
        }, 1000);
    }
    startTick();
}

function ResetResend() {
    $("#btnResendOTP").removeClass("hidden");
    $("#timersection").addClass("hidden");
}

function ResendOTP() {
    $("#otperrormessage").html("");
    $("#btnResendOTP").html("Wait... <i class='fa fa-pulse fa-spinner'></i>");
    var hdnremtVal = $("#hdnRegRemtId").val();

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/dmt-manager/dmt-direct.aspx/DMTDResendOtpToRemitterMobile",
        data: '{mobileno: ' + JSON.stringify(regmobile) + ',remitterid: ' + JSON.stringify(hdnremtVal) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "otpsent") {
                    $("#otperrormessage").html("OTP sent.");
                    StartCountDown(30);
                }
                else {
                    $("#otperrormessage").html(data.d[1]);
                }
            }
            $("#btnResendOTP").html("Resend OTP");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

//=============Update Address=====================
$(document.body).on('click', "#UpdateLocalAddress", function (e) {
    $("#updatemsg").html("");
    $(".updateaddresspopup").click();
});

function UpdateCurrLocalAddress() {
    $("#updatemsg").html("");
    var thisbutton = $("#btnUpdateCurrLocalAddress");
    if (CheckFocusBlankValidation("txtUpdateCurrLocalAddress")) return !1;

    var address = $("#txtUpdateCurrLocalAddress").val();
    if (address != "") {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/UpdateLocalAddress",
            data: '{updatedaddress: ' + JSON.stringify(address) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#txtUpdateCurrLocalAddress").val("");
                        $("#updatemsg").html(data.d[1]);
                    }
                    else {
                        $("#updatemsg").html(data.d[1]);
                    }
                }
                $(thisbutton).html("Update Address");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

$("#UpdateAddressClose").click(function () {
    window.location.reload();
});

//=============Add New Beneficiary=====================
function AddNewReceiver() {
    $("#txtBenAccountNo").val("");
    $("#txtBenIFSCNo").val("");
    $("#txtBenPerName").val("");
    //$("#txtBenPerMobile").val("");
    $("#benerrormessage").html("");
    $("#bensuccessmessage").html("");
    $(".addbenificiarydetail").click();
    BindAllBank();
}

function BindAllBank() {
    $("#ProcessBindingBank").removeClass("hidden");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SenderDetails.aspx/BindAllBank",
        //data: '{mobileno: ' + JSON.stringify(mobilenumber) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d != "") {
                    $("#ddlBindBankDrop").html("");
                    $("#ddlBindBankDrop").html(data.d);
                }
            }
            $("#ProcessBindingBank").addClass("hidden");
        },
        failure: function (response) { alert("failed"); }
    });
}

function GetBeneficiaryNamePayout() {
    if (CheckFocusBlankValidation("txtBenAccountNo")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtBenIFSCNo")) return !1;

    var accountno = $("#txtBenAccountNo").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var transfertype = $("#ddlBindBankDrop option:selected").data("transfertype");
    var ifsccode = $("#txtBenIFSCNo").val();
    var paytype = "paygetname";

    if (confirm("Are you sure you want to get beneficiary detail, It will charge ₹ 3 ?")) {
        $("#CheckAccountDetailPayout").html("featching...<i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GetBeneficiaryNamePayout",
            data: '{accountno: ' + JSON.stringify(accountno) + ',transtype: ' + JSON.stringify(transfertype) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',bankname:' + JSON.stringify(bankname) + ',paytype:' + JSON.stringify(paytype) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#txtBenPerName").val(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#benerrormessage").html("Error: " + data.d[1]);
                    }
                }
                $("#CheckAccountDetailPayout").html("Get Beneficiary Name");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function SubmitBeneficiaryDetail() {
    $("#benerrormessage").html("");
    $("#bensuccessmessage").html("");

    var thisbutton = $("#btnBenRegistration");

    if (CheckFocusBlankValidation("txtBenAccountNo")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtBenIFSCNo")) return !1;
    if (CheckFocusBlankValidation("txtBenPerName")) return !1;
    //if (CheckFocusBlankValidation("txtBenPerMobile")) return !1;

    var accountno = $("#txtBenAccountNo").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var ifsccode = $("#txtBenIFSCNo").val();
    var name = $("#txtBenPerName").val();
    //var mobile = $("#txtBenPerMobile").val();

    if (confirm("Are you sure you want to add beneficiary ?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/PayoutBeneficiaryRegistration",
            //data: '{benmobile: ' + JSON.stringify(mobile) + ',accountno: ' + JSON.stringify(accountno) + ',bankname: ' + JSON.stringify(bankname) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',name: ' + JSON.stringify(name) + '}',
            data: '{accountno: ' + JSON.stringify(accountno) + ',bankname: ' + JSON.stringify(bankname) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',name: ' + JSON.stringify(name) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#BeneficaryRowDetails").html("");
                        $("#BeneficaryRowDetails").html(data.d[1]);
                        $("#bensuccessmessage").html("Beneficiary has been added successfully.");
                        ResetBeneficiaryFields();
                        //$('#FormBeneficiary').modal('hide');

                        //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Beneficiary has been added successfully.", "#28a745");
                    }
                    else if (data.d[0] == "failed" || data.d[0] == "error") {
                        $("#benerrormessage").html(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                }
                $(thisbutton).html("Submit");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function ResetBeneficiaryFields() {
    $("#txtBenAccountNo").val("");
    $("#txtBenIFSCNo").val("");
    $("#txtBenPerName").val("");
    //$("#txtBenPerMobile").val("");
    $("#typeofifsc").html("");
    $("#txtBenIFSCNo").val("");
    $("#ddlBindBankDrop").val("").change();
}

function DirectTransferMoney(benid) {
    if (benid != null) {
        $("#FundTransDetailBody").html("");
        $("#FundTransDetailFooter").html("");
        $("#FundTransDetailFooter").removeClass("hidden");
        $("#paratranserror").html("");

        $(".form-validation").each(function () { $(this).removeClass("has-error"); });

        var thisbutton = $("#btnMoneyTransfer_" + benid);

        if (CheckFocusBlankValidation("txtTranfAmount_" + benid)) return !1;
        var amount = $("#txtTranfAmount_" + benid).val();
        var mode = $("#ddlTransferMode_" + benid + " option:selected").text();
        var modetype = $("#ddlTransferMode_" + benid + " option:selected").val();

        if (amount != "") {
            $(thisbutton).html("Wait..<i class='fa fa-pulse fa-spinner'></i>");

            $.ajax({
                type: "Post",
                contentType: "application/json; charset=utf-8",
                url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GetPayoutFundTransferVeryficationDetail",
                data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + ',mode: ' + JSON.stringify(mode) + ',modetype: ' + JSON.stringify(modetype) + '}',
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data.d[0] == "success") {
                            $("#FundTransDetailBody").html(data.d[1]);
                            $("#FundTransDetailFooter").html(data.d[2]);
                            $(".VerifyTransferDetails").click();
                        }

                        $("#txtTranfAmount_" + benid).val("");
                        $("#ddlTransferMode_" + benid).prop('selectedIndex', 'DPN').change();
                    }
                    $(thisbutton).html("Transfer");
                },
                failure: function (response) {
                    alert("failed");
                }
            });
        }
    }
}

function PayoutFundTransfer() {
    $("#paratranserror").html("").removeClass("text-danger");
    var benid = $("#btnIMPSTrans").data("benid");
    var amount = $("#btnIMPSTrans").data("transamount");
    var transmode = $("#btnIMPSTrans").data("modetype");
    var acceptdeclar = false;

    if ($("#ChkFundTransCheckboxCheck").prop("checked") == true) { acceptdeclar = true; } else {
        $("#paratranserror").html("Please click on checkbox for accept the above declaraction.").addClass("text-danger");;
    }
    if (acceptdeclar) {
        //ProcessToFundTransfer(benid, amount, transmode, $("#btnIMPSTrans"), $("#btnNEFTTrans"), "DIRECT FUND TRANSFER");

        GenrateOTPForTransfer(benid, amount, transmode, $("#btnIMPSTrans"), "DIRECT FUND TRANSFER");
    }
}

function GenrateOTPForTransfer(benid, amount, transmode, btnid, btnname) {
    $(btnid).html("Processing..<i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
    $("#paratranserror").html("");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GenrateOTPForTransfer",
        data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + ',transmode: ' + JSON.stringify(transmode) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#FundTransDetailFooter").html("");
                    $("#FundTransDetailFooter").html(data.d[1]);
                    PayoutStartCountDown(30);
                }
                else {
                    $("#paratranserror").html(data.d[1]).addClass("text-danger");
                }
            }
            $(btnid).html(btnname).prop('disabled', false);
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

var _paytick = null;
function PayoutStartCountDown(startSecond) {
    $("#btnPayoutResendOTP").addClass("hidden");
    $("#payouttimersection").removeClass("hidden");

    clearInterval(_paytick);
    $("#paysecRemaing").html("");
    $("#payminRemaing").html("");
    $(".strcountdown").removeClass("hidden");
    var remSeconds = startSecond;
    var secondCounters = remSeconds % 60;
    function formarteNumber(number) { if (number < 10) return '0' + number; else return '' + number; }
    function startTick() {
        $("#paysecRemaing").text(formarteNumber(secondCounters));
        $("#payminRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
        if (secondCounters == 0) { secondCounters = 60; }
        _paytick = setInterval(function () {
            if (remSeconds > 0) {
                remSeconds = remSeconds - 1;
                secondCounters = secondCounters - 1;
                $("#paysecRemaing").text(formarteNumber(secondCounters));
                $("#payminRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
                if (secondCounters == 0) { secondCounters = 60; }
            }
            else {
                clearInterval(_paytick);
                PayResetResend();
            }
        }, 1000);
    }
    startTick();
}

function PayResetResend() {
    $("#btnPayoutResendOTP").removeClass("hidden");
    $("#payouttimersection").addClass("hidden");
}

function ProcessToFundTransfer(benid, amount, transmode, btnid, btnname, otp, remark) {
    $(btnid).html("Processing..<i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);

    $("#paratranserror").html("");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/ProcessToPayoutFundTransfer",
        data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + ',transmode: ' + JSON.stringify(transmode) + ',otp: ' + JSON.stringify(otp) + ',remark: ' + JSON.stringify(remark) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#FundTransDetailBody").html("");
                    $("#FundTransDetailBody").html(data.d[1]);
                    $("#FundTransDetailFooter").html("");
                    $("#FundTransDetailFooter").addClass("hidden");
                    $("#VerifyTransferModalDialog").removeAttr("style").css("margin", "3% auto");

                    //$("#RemitterDetailsSection").html("");
                    //$("#RemitterDetailsSection").html(data.d[2]);
                }
                else if (data.d[0] == "otpexp" || data.d[0] == "failed") {
                    $("#paratranserror").html(data.d[1]).addClass("text-danger");
                    $("#txtPayoutOTPInput").val("");
                }
                else if (data.d[0] == "reload") {
                    window.href.reload();
                }
            }
            $(btnid).html(btnname).prop('disabled', false);
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function PayoutResendOTP() {
    $("#paratranserror").html("");
    var benid = $("#btnPayoutResendOTP").data("benid");
    var transamount = $("#btnPayoutResendOTP").data("transamount");
    var modetype = $("#btnPayoutResendOTP").data("modetype");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GenrateOTPForTransfer",
        data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(transamount) + ',transmode: ' + JSON.stringify(modetype) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#FundTransDetailFooter").html("");
                    $("#FundTransDetailFooter").html(data.d[1]);
                    $("#paratranserror").html("OTP sent successfully.");
                    PayoutStartCountDown(30);
                }
                else {
                    $("#paratranserror").html(data.d[1]).addClass("text-danger");
                }
            }
            $(btnid).html(btnname).prop('disabled', false);
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

$(document.body).on('click', "#btnPayoutOTPTrans", function (e) {
    if (CheckFocusBlankValidation("txtRemtRemark")) return !1;
    if (CheckFocusBlankValidation("txtPayoutOTPInput")) return !1;

    var otp = $("#txtPayoutOTPInput").val();
    var benid = $(this).data("benid");
    var transamount = $(this).data("transamount");
    var modetype = $(this).data("modetype");
    var remark = $("#txtRemtRemark").val();

    ProcessToFundTransfer(benid, transamount, modetype, $("#btnPayoutOTPTrans"), "TRANSFER", otp, remark);
});

//=================================dmt-direct-fund-transfer.aspx=========================
function SubmitPayoutKYCDetails() {
    //var thisbutton = $("#btnPayoutSubmitKYC");

    if (CheckFocusBlankValidation("txtPayoutFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPayoutLastName")) return !1;
    //if (CheckFocusBlankValidation("txtPayoutAddress")) return !1;
    //if (CheckFocusDropDownBlankValidation("ddlPayoutGender")) return !1;
    //if (CheckFocusBlankValidation("txtPayoutDOB")) return !1;
    //if (CheckFocusDropDownBlankValidation("ddlPayoutProof")) return !1;
    //if (CheckFocusBlankValidation("txtProofNumber")) return !1;
    //var frontimg = $("#fluPayoutFrontImg").get(0).files.length;
    //if (frontimg == 0) { $("#fluPayoutFrontImg").css("border", "1px solid red").css("padding", "5px"); return false; } else { $("#fluPayoutFrontImg").removeAttr("style"); }
    //var frontimg = $("#fluPayoutBackImg").get(0).files.length;
    //if (frontimg == 0) { $("#fluPayoutBackImg").css("border", "1px solid red").css("padding", "5px"); return false; } else { $("#fluPayoutBackImg").removeAttr("style"); }
    //if ($('#chkPayoutDeclaration').is(":checked") == false) { alert("Please check declaration option"); return false; }

    //var firstname = $("#txtPayoutFirstName").val();
    //var lastname = $("#txtPayoutLastName").val();
    //var address = $("#txtPayoutAddress").val();
    //var gender = $("#ddlPayoutGender option:selected").val();
    //var dob = $("#txtPayoutDOB").val();
    //var proof = $("#ddlPayoutProof option:selected").val();
    //var proofno = $("#txtProofNumber").val();

    //$(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);

    //$.ajax({
    //    type: "Post",
    //    contentType: "application/json; charset=utf-8",
    //    url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/SubmitKycDetails",
    //    data: '{fn: ' + JSON.stringify(firstname) + ',ln: ' + JSON.stringify(lastname) + ',add: ' + JSON.stringify(address) + ',gen: ' + JSON.stringify(gender) + ',dob: ' + JSON.stringify(dob) + ',proof: ' + JSON.stringify(proof) + ',pno: ' + JSON.stringify(proofno) + '}',
    //    datatype: "json",
    //    success: function (data) {
    //        if (data != null) {
    //            if (data.d == "success") {
    //            }
    //            else {
    //                alert(data.d);
    //            }
    //        }
    //        $(thisbutton).html("Submit KYC Details");
    //    },
    //    failure: function (response) {
    //        alert("failed");
    //    }
    //});
}

function SubmitKYCImages() {
    var files = input.files.length;

    var fileData = new FormData();
    for (var i = 0; i < files.length; i++) {
        fileData.append(files[i].name, files[i]);
    }

    $.ajax({
        url: '/DMT-Manager/dmt-direct-fund-transfer.aspx/SubmitKycImageDetails',
        type: "POST",
        contentType: false, // Not to set any content header  
        processData: false, // Not to process data  
        data: fileData,
        success: function (result) {
            if (result != "") {

            }
        }
    });
}

$("#TransHistory").click(function () {
    PayoutClearTransFilter();
});

function PayoutClearTransFilter() {
    $("#btnPayoutTransFilter").html("Search");

    $("#txtPayoutTransFromDate").val("");
    $("#txtPayoutTransToDate").val("");
    $("#txtPayoutTransTrackId").val("");
    $("#ddlPayoutTransStatus").prop('selectedIndex', '').change();
    BindTransAllDetails();
}

function BindTransAllDetails() {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GetTransactionHistory",
        //data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#PayoutFundTransRowDetails").html("");
                    $("#PayoutFundTransRowDetails").html(data.d[1]);
                }
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function TransFilter() {
    var thisbutton = $("#btnTransFilter");

    var fromdate = $("#txtTransFromDate").val();
    var todate = $("#txtTransToDate").val();
    var trackid = $("#txtTransTrackId").val();
    var filstatus = $("#ddlTransStatus option:selected").val();

    $(thisbutton).html("Search... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GetFilterTransactionHistory",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',trackid: ' + JSON.stringify(trackid) + ',filstatus: ' + JSON.stringify(filstatus) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#PayoutFundTransRowDetails").html("");

                    $("#PayoutFundTransRowDetails").html(data.d[1]);
                }
            }
            $(thisbutton).html("Search");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function ShowBenDetails(benid) {
    if (benid != null) {
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GetBeneficiaryDetailById",
            data: '{benid: ' + JSON.stringify(benid) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != "") {
                    $(".benificierypopupclass").click();
                    $("#BenBodyContent").html(data.d);
                }
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function PayoutTransFilter() {
    var thisbutton = $("#btnPayoutTransFilter");

    var fromdate = $("#txtPayoutTransFromDate").val();
    var todate = $("#txtPayoutTransToDate").val();
    var trackid = $("#txtPayoutTransTrackId").val();
    var filstatus = $("#ddlPayoutTransStatus option:selected").val();

    $(thisbutton).html("Search... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GetFilterTransactionHistory",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',trackid: ' + JSON.stringify(trackid) + ',filstatus: ' + JSON.stringify(filstatus) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#PayoutFundTransRowDetails").html("");

                    $("#PayoutFundTransRowDetails").html(data.d[1]);
                }
            }
            $(thisbutton).html("Search");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

var deletedbenid = "";
function DeleteBeneficialRecord(benid) {
    if (benid != "") {
        deletedbenid = benid;
        $("#hdnDeleteBenId").val(benid);
        $("#bendeletemsg").html("");
        $("#ConfirmBenDelete").removeClass("hidden");
        $("#DeleteSuccessfully").addClass("hidden");
        $("#OTPConfirmBenDelete").addClass("hidden");
        $("#txtBenDeleteOTP").val("");
        $("#btnOTPDelConfirmed").html("DELETE");
        $("#checkotperror").html("");
        $("#btnDelConfirmed").html("YES");
        $(".delbendetail").click();
    }
}

function YesConfirmDeleteDen() {
    $("#btnDelConfirmed").html("<i class='fa fa-pulse fa-spinner'></i>");
    var benid = $("#hdnDeleteBenId").val();
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/GenrateDeleteBeneficiaryOTP",
        data: '{benid: ' + JSON.stringify(benid) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#ConfirmBenDelete").addClass("hidden");
                    $("#DeleteSuccessfully").addClass("hidden");
                    $("#OTPConfirmBenDelete").removeClass("hidden");
                }
                else {
                    $("#checkotperror").html(data.d[1]);
                }
            }

            $("#YesConfirmDeleteDen").html("YES");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function ConfirmDeleteDen() {
    $("#checkotperror").html("");
    if (CheckFocusBlankValidation("txtBenDeleteOTP")) return !1;

    var benid = $("#hdnDeleteBenId").val();
    var delotp = $("#txtBenDeleteOTP").val();
    $("#btnOTPDelConfirmed").html("<i class='fa fa-pulse fa-spinner'></i>");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/dmt-direct-fund-transfer.aspx/DeleteBeneficiaryDetailById",
        data: '{benid: ' + JSON.stringify(benid) + ',otp: ' + JSON.stringify(delotp) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#ConfirmBenDelete").addClass("hidden");
                    $("#OTPConfirmBenDelete").addClass("hidden");
                    $("#DeleteSuccessfully").removeClass("hidden");
                    $("#bendeletemsg").html("Beneficary detail has been deleted successfully.");

                    $("#BeneficaryRowDetails").html("");
                    $("#BeneficaryRowDetails").html(data.d[1]);
                }
                else {
                    $("#checkotperror").html(data.d[1]);
                }
            }
            $("#btnOTPDelConfirmed").html("DELETE");
            $("#btnDelConfirmed").html("YES");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

$("#ddlBindBankDrop").change(function () {
    $("#typeofifsc").html("");
    $("#txtBenIFSCNo").val("");
    var selectedifsccode = $("#ddlBindBankDrop option:selected").val();
    if (selectedifsccode != null && selectedifsccode != "") {
        $("#typeofifsc").html("universal ifsc code.");
        $("#txtBenIFSCNo").val(selectedifsccode);
    }
});