﻿//var mobilenoid; var searchbtnid;
var regmobile; var regfirstname; var reglastname; var regpincode; var regbutton;

var regRemitterId = null;

function RemitterMobileSearch() {
    $("#RemitterDetailsByMobile").html("");
    $("#BindBenificiary").html("");
    $("#BenificiarySection").addClass("hidden");
    $("#ViewAllHistorySection").addClass("hidden");
    $("#AllTransHistoryDetail").addClass("hidden");

    var thisbutton = $("#btnRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtSenderMobileNo")) return !1;
    var mobileno = $("#txtSenderMobileNo").val();
    if (mobileno.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SendMoney.aspx/RemitterMobileSearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    var datalength = data.d.length;
                    if (data.d[0] == "success") {
                        $("#RemitterDetailsByMobile").html(data.d[1]);
                        regRemitterId = $("#hdnRemiterId").val();
                        if (datalength > 2) {
                            $("#BindBenificiary").html(data.d[2]);
                            $("#BenificiarySection").removeClass("hidden");
                            $("#ViewAllHistorySection").removeClass("hidden");
                        }
                        if (datalength > 3) {
                            $("#BindTransDeails").html(data.d[3]);
                        }
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                    }
                }
                $(thisbutton).html("Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", "Mobile number not valid.", "#d91717");
    }
}

function CompleteYourKYC() {
    alert();
}

function CheckRegRemitter() {
    $("#perrormessage").html("");
    var thisbutton = $("#btnRemtrRegistration");

    if (CheckFocusBlankValidation(regmobile)) return !1;
    if (CheckFocusBlankValidation(regfirstname)) return !1;
    if (CheckFocusBlankValidation(reglastname)) return !1;
    if (CheckFocusBlankValidation(regpincode)) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var mobile = $("#" + regmobile).val();
    var firstname = $("#" + regfirstname).val();
    var lastname = $("#" + reglastname).val();
    var pincode = $("#" + regpincode).val();


    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/RemitterRegistration",
        data: '{mobile: ' + JSON.stringify(mobile) + ',firstname: ' + JSON.stringify(firstname) + ',lastname: ' + JSON.stringify(lastname) + ',pincode: ' + JSON.stringify(pincode) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "success") {
                    regRemitterId = data.d[1];
                    $("#RegistSectionForm").addClass("hidden");
                    $("#OTPSectionForm").removeClass("hidden");
                }
                else if (data.d[0] == "failed") {
                    $("#perrormessage").html(data.d[1]);
                }
                else if (data.d[0] == "reload") {
                    window.location.reload();
                }
                else {
                    $("#otppopclose").click();
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                }
            }
            $(thisbutton).html("Register");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function CheckRegRemitterVerification() {
    var thisbutton = $("#btnRegistrationVarification");
    if (CheckFocusBlankValidation("txtOtp")) return !1;
    var mobile = $("#txtSenderMobileNo").val();
    var enteredotp = $("#txtOtp").val();
    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    var remitterid = regRemitterId;

    if (mobile != null || mobile != undefined) {
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SendMoney.aspx/RemitterVarification",
            data: '{mobile: ' + JSON.stringify(mobile) + ',remitterid: ' + JSON.stringify(remitterid) + ',otp: ' + JSON.stringify(enteredotp) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] != "error") {
                        $("#btnregformclose").click();
                        ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#d91717", "Remitter registration has been successfully completed. Please search mobile number again.", "#d91717");
                    }
                    else {
                        $("#btnregformclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                    }
                }
                $(thisbutton).html("Verify");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        alert("Error : Search Again!");
    }
}

//$("#btnregformclose").click(function () { window.location.reload(); });

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".successmessage").click();
    $(".modelheading").css("color", headcolor).html(headerHeading);
    $(".sucessmsg").css("color", bodycolor).html(bodyMsg);
}

//$(".otppopclose").click(function () {
//    window.location.reload();
//});

//===============================AddBeneficiaryDetail====================================
function AddNewBenDetail() {
    $("#txtBenAccountNo").val("");
    $("#txtBenIFSCNo").val("");
    $("#txtBenPerName").val("");
    $("#txtBenPerMobile").val("");
    $("#btnBenRegistration").html("Submit");

    $("#BeneficiarySectionForm").removeClass("hidden");
    $("#OTPBeneficiarySectionForm").addClass("hidden");
    $(".addbenificiarydetail").click();
}

function AddBeneficiaryDetail() {
    $("#benerrormessage").html("");

    var thisbutton = $("#btnBenRegistration");
    //if (CheckFocusDropDownBlankValidation("ddlBenBank")) return !1;
    if (CheckFocusBlankValidation("txtBenAccountNo")) return !1;
    if (CheckFocusBlankValidation("txtBenIFSCNo")) return !1;
    if (CheckFocusBlankValidation("txtBenPerName")) return !1;
    if (CheckFocusBlankValidation("txtBenPerMobile")) return !1;

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    //var bank = $("#" + regmobile).val();
    var accountno = $("#txtBenAccountNo").val();
    var ifsccode = $("#txtBenIFSCNo").val();
    var name = $("#txtBenPerName").val();
    var mobile = $("#txtBenPerMobile").val();
    var remtmobile = $("#txtSenderMobileNo").val();
    var regremitid = regRemitterId;

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/BeneficiaryRegistration",
        data: '{remtmobile:' + JSON.stringify(remtmobile) + ',mobile: ' + JSON.stringify(mobile) + ',accountno: ' + JSON.stringify(accountno) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',name: ' + JSON.stringify(name) + ',remitterid: ' + JSON.stringify(regremitid) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "success") {
                    $("#BindBenificiary").html("");
                    $("#BindBenificiary").html(data.d[1]);

                    $("#BeneficiarySectionForm").addClass("hidden");
                    $("#OTPBeneficiarySectionForm").removeClass("hidden");
                }
                else if (data.d[0] == "failed") {
                    $("#benerrormessage").html(data.d[1]);
                }
                else {
                    $("#benotppopclose").click();
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                }
            }
            $(thisbutton).html("Submit");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

//$("#CloseBenPopUp").click(function () {
//    RemitterMobileSearch();
//});

var sendmoney = {};
function SendMoneyEvent(beenid) {
    var thisid = $("#SendMoney_" + beenid);
    sendmoney.benid = beenid;
    sendmoney.remtmobile = $("#txtSenderMobileNo").val();
    sendmoney.benmobile = $(thisid).data("benmobile");
    sendmoney.benname = $(thisid).data("benname");
    sendmoney.benbank = $(thisid).data("benbank");
    sendmoney.benaccountno = $(thisid).data("benaccountno");

    $("#SendMoneyBenName").html(sendmoney.benname);
    $("#SendMoneyBenAccNo").html(sendmoney.benaccountno);
    $("#SendMoneyBenBankName").html(sendmoney.benbank);

    $("#allTransactions").prop("checked", true);
    $("#txtBenAmount").val("");

    $(".moneytransfer").click();
}

function DeleteBenEvent(beenid) {
    $("#ConfirmBenDelete").removeClass("hidden");
    $("#BenDeleteOtpSent").addClass("hidden");
    $("#txtBenDeleteOtp").val("");
    $("#btnBenDelVarification").html("Verify");

    var thisid = $("#DeleteBen_" + beenid);
    sendmoney.benid = beenid;
    sendmoney.benmobile = $(thisid).data("benmobile");
    sendmoney.benname = $(thisid).data("benname");
    sendmoney.benbank = $(thisid).data("benbank");
    sendmoney.benaccountno = $(thisid).data("benaccountno");
    $(".delbendetail").click();
}

function ConfirmDeleteDen() {
    var remitterid = regRemitterId;
    var benificiaryId = sendmoney.benid;
    $("#btnDelConfirmed").html("<i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/DeleteBeneficiary",
        data: '{remitterid: ' + JSON.stringify(remitterid) + ',benificiaryid: ' + JSON.stringify(benificiaryId) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "success") {
                    $("#ConfirmBenDelete").addClass("hidden");
                    $("#BenDeleteOtpSent").removeClass("hidden");
                }
                else {
                    $("#BenDelCloseClick").click();
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                }
            }
			$("#btnDelConfirmed").html("Yes");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function BenDelVarification() {
    var thisbutton = $("#btnBenDelVarification");
    var remitterid = regRemitterId;
    var benificiaryId = sendmoney.benid;

    if (CheckFocusBlankValidation("txtBenDeleteOtp")) return !1;

    var otp = $("#txtBenDeleteOtp").val();
    var mobile = $("#txtSenderMobileNo").val();

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/DeleteBeneficiaryVarification",
        data: '{mobile:' + JSON.stringify(mobile) + ',remitterid: ' + JSON.stringify(remitterid) + ',benificiaryid: ' + JSON.stringify(benificiaryId) + ',otp: ' + JSON.stringify(otp) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "success") {
                    $("#BindBenificiary").html("");
                    $("#BindBenificiary").html(data.d[1]);
                    $("#BenificiarySection").removeClass("hidden");

                    $("#BenDelCloseClick").click();
                    ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Beneficiary has been deleted successfully.", "#28a745");
                }
                else {
                    $("#BenDelCloseClick").click();
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                }
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function BenSendMoneyEvent() {
    var thisbutton = $("#btnBenSendMoney");
    if (CheckFocusBlankValidation("txtBenAmount")) return !1;
    var benAmount = $("#txtBenAmount").val();
    var txnmode = $("input[name='sendtype']:checked").val();

    $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

    $("#errortranfermoney").html("");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/MoneyTransfer",
        data: '{remtmobile: ' + JSON.stringify(sendmoney.remtmobile) + ',benid: ' + JSON.stringify(sendmoney.benid) + ',amount: ' + JSON.stringify(benAmount) + ',txnmode: ' + JSON.stringify(txnmode) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "success") {
                    $("#BindTransDeails").html(""); $("#BindTransDeails").html(data.d[2]);
                    $("#sendmoneypopup").click();
                    ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#218838", data.d[1], "#218838");
                }
                else if (data.d[0] == "failed") {
                    $("#sendmoneypopup").click();
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                }
                else if (data.d[0] == "mismatch") {
                    $("#errortranfermoney").html(data.d[1]);
                }
                else if (data.d[0] == "reload") {
                    window.location.reload();
                }
                else {
                    $("#sendmoneypopup").click();
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                }
            }
            $(thisbutton).html("Transfer");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

var isfirsttimetransbind = false;
function ViewAllTansClick() {
    var mobile = $("#txtSenderMobileNo").val();

    if (mobile != null && mobile != undefined) {
        if (!isfirsttimetransbind) {
            $("#btnViewAllTans").html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

            $.ajax({
                type: "Post",
                contentType: "application/json; charset=utf-8",
                url: "/DMT-Manager/SendMoney.aspx/GetMoneyTransferDel",
                data: '{rmtmobile:' + JSON.stringify(mobile) + '}',
                datatype: "json",
                success: function (data) {
                    if (data.d != null) {
                        $("#BindTransDeails").html(data.d);

                        $("#AllTransHistoryDetail").removeClass("hidden");
                        $("#BenificiarySection").addClass("hidden");
                        isfirsttimetransbind = true;
                    }
                    $("#btnViewAllTans").html("<i class='fa fa-eye aria-hidden='true'></i>&nbsp;View Transaction History");
                },
                failure: function (response) {
                    alert("failed");
                }
            });
        }
        else {
            $("#AllTransHistoryDetail").removeClass("hidden");
            $("#BenificiarySection").addClass("hidden");
        }
    }
    else {
        window.location.reload();
    }
}

function ViewAllBenDelClick() {
    var mobile = $("#txtSenderMobileNo").val();

    if (mobile != null && mobile != undefined) {
        $("#AllTransHistoryDetail").addClass("hidden");
        $("#BenificiarySection").removeClass("hidden");

        //$.ajax({
        //    type: "Post",
        //    contentType: "application/json; charset=utf-8",
        //    url: "/DMT-Manager/SendMoney.aspx/BindBeneficiaryDetails",
        //    data: '{mobile:' + JSON.stringify(mobile) + '}',
        //    datatype: "json",
        //    success: function (data) {
        //        if (data != null) {
        //            $("#BindBenificiary").html("");
        //            $("#BindBenificiary").html(data.d);

        //            $("#AllTransHistoryDetail").addClass("hidden");
        //            $("#BenificiarySection").removeClass("hidden");
        //        }
        //    },
        //    failure: function (response) {
        //        alert("failed");
        //    }
        //});
    }
    else {
        window.location.reload();
    }
}

//======================Filter=========================
function TransFilterSearch() {
    var thisbutton = $("#btnTransFilterSearch");

    var fromdate = $("#txtFilterFromDate").val();
    var todate = $("#txtFilterToDate").val();
    var transid = $("#txtFilterTransId").val();
    var trackid = $("#txtFilterTrackId").val();

    $(thisbutton).html("Searching... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/FilterTransDetails",
        data: '{fromdate:' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',transid: ' + JSON.stringify(transid) + ',trackid: ' + JSON.stringify(trackid) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d != "reload") {
                    if (data.d == "empty") {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", "Please select any one field for search !", "#d91717");
                    }
                    else {
                        $("#BindTransDeails").html("");
                        $("#BindTransDeails").html(data.d);
                        $("#btnCancelTransFilterSearch").removeClass("hidden");
                    }
                }
                else {
                    window.location.reload();
                }
            }
            $(thisbutton).html("Search");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function CancelTransFilterSearch() {
    $("#btnCancelTransFilterSearch").html("<i class='fa fa-pulse fa-spinner'></i>");
    var mobile = $("#txtSenderMobileNo").val();

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SendMoney.aspx/GetMoneyTransferDel",
        data: '{rmtmobile:' + JSON.stringify(mobile) + '}',
        datatype: "json",
        success: function (data) {
            $("#BindTransDeails").html("");
            $("#BindTransDeails").html(data.d);
            $("#btnCancelTransFilterSearch").addClass("hidden");

            $("#txtFilterFromDate").val("");
            $("#txtFilterToDate").val("");
            $("#txtFilterTransId").val("");
            $("#txtFilterTrackId").val("");
            $("#filtercollapsed").click();
            $("#btnCancelTransFilterSearch").html("X");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

$('.CommanDate').datepicker({
    dateFormat: "dd/mm/yy",
    showStatus: true,
    showWeeks: true,
    currentText: 'Now',
    autoSize: true,
    maxDate: -0,
    gotoCurrent: true,
    showAnim: 'blind',
    highlightWeek: true

});