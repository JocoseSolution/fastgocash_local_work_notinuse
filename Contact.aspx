﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="DMT_Manager_Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <section class="page-header page-header-text-light bg-dark-3 py-5">
  <div class="container">
    <div class="row text-center">
      <div class="col-12">
        <ul class="breadcrumb mb-0">
          <li><a href="DMT-Home.aspx">Home</a></li>
          <li class="active">Contact Us</li>
        </ul>
      </div>
      <div class="col-12">
        <h1>Contact Us</h1>
      </div>
    </div>
  </div>
</section>

    <div id="content">
  <div class="container">
      <div class="row">
      
      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
      	<div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-map-marker-alt"></i></div>
              <h3>FastGoCash Inc.</h3>
              <p>4th Floor, New Delhi<br>
                145 Murphy Canyon Rd.<br>
                Suite 100-18<br>
                New Delhi, Delhi </p>
            </div>
      </div>
      </div>
      
      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
      	<div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-phone"></i> </div>
              <h3>Telephone</h3>
              <p class="mb-0">(+91) XXXXXXXXXX</p>
              <p>(+91) XXXXXXXXXX</p>
            </div>
      </div>
      </div>
      
      <div class="col-md-4 mb-4">
      <div class="bg-white shadow-md rounded h-100 p-3">
      	<div class="featured-box text-center">
              <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-envelope"></i> </div>
              <h3>Business Inquiries</h3>
              <p>info@fastgocash.com</p>
            </div>
      </div>
      </div>
      
      
      <div class="col-12 mb-4">
      <div class="text-center py-5 px-2">
      <h2 class="text-8">Get in touch</h2>
      <p class="lead">Connect with our social app to get new updates.</p>
      	<div class="d-flex flex-column">
              <ul class="social-icons social-icons-lg social-icons-colored justify-content-center">
                <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="" data-original-title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="" data-original-title="Twitter"><i class="fab fa-twitter"></i></a></li>
                <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="" data-original-title="Google"><i class="fab fa-google"></i></a></li>
                <li class="social-icons-linkedin"><a data-toggle="tooltip" href="http://www.linkedin.com/" target="_blank" title="" data-original-title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="" data-original-title="Youtube"><i class="fab fa-youtube"></i></a></li>
                <li class="social-icons-instagram"><a data-toggle="tooltip" href="http://www.instagram.com/" target="_blank" title="" data-original-title="Instagram"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
      </div>
      </div>
      
      </div>
    </div>
    
    
    
    



</div>

</asp:Content>

